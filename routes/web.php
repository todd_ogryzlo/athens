<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('room-home');
Route::get('/find-room', 'RoomsController@roomiesRoom')->name('find-room');
Route::post('/find-room', 'RoomsController@prospectiveRenters')->name('prospective_renters');
Route::post('/prospective-login', 'RoomsController@prospectiveRentersLogin')->name('renters-login');
Route::post('/house-manager-reg', 'HouseManagerController@houseManagerReg')->name('house-manager-reg');
Route::post('/house-manager-login', 'HouseManagerController@houseManagerLogin')->name('house-manager-login');
Route::get('/logout','Auth\LoginController@logout')->name('logout');
Route::get('/feedback', 'FeedbackController@index')->name('feedback');
Route::post('/feedback', 'FeedbackController@store');

// Route::get('/test-login','Auth\LoginController@test')->name('test-login');


Route::middleware('auth')->group( function(){
    Route::get('/dashboard','DashboardController@index')->name('dashboard');
    Route::post('interested', 'DashboardController@interestedRenters');
    Route::get('/edit-profile','ProfileController@edit_profile')->name('profile');
    Route::post('/edit_profile','ProfileController@store');
    Route::get('/viewings', 'ShowingsController@createViewingTime')->name('viewing-time');
    Route::get('/viewings/edit/{schedule}', 'ShowingsController@editViewingTime');
    Route::post('/viewing-time/edit', 'ShowingsController@updateViewingTime')->name('update-viewing');
    Route::post('/viewing-time/{schedule}/delete', 'ShowingsController@deleteSchedule');
    Route::post('/viewing-time', 'ShowingsController@storeViewingTime')->name('store-viewing');
    Route::get('/schedule','ShowingsController@schedule')->name('schedule');
    // Route::get('/', 'ContentsController@home')->name('home');
    Route::get('/clients', 'ClientController@index')->name('clients');
    Route::get('/clients/new', 'ClientController@newClient')->name('new_client');
    Route::post('/clients/new', 'ClientController@newClient')->name('create_client');
    Route::get('/clients/{client_id}', 'ClientController@show')->name('show_client');
    Route::post('/clients/{client_id}', 'ClientController@modify')->name('update_client');

    Route::get('/reservations/{client_id}', 'RoomsController@checkAvailableRooms')->name('check_room');
    Route::post('/reservations/{client_id}', 'RoomsController@checkAvailableRooms')->name('check_room');

    Route::get('/book/room/{client_id}/{room_id}/{date_in}/{date_out}', 'ReservationsController@bookRoom')->name('book_room');
    Route::get('export', 'ClientController@export');
    Route::get('/upload', 'ContentsController@upload')->name('upload');
    Route::post('/upload', 'ContentsController@upload')->name('upload');
});

Route::get('/logout','Auth\LoginController@logout');

Route::get('/about', function () {
    $response_arr = [];
    $response_arr['author'] = 'BP';
    $response_arr['version'] = '0.1.1';
    return $response_arr;
    //return '<h3>About</h3>';
});

// Route::get('/home', function () {
//     $data = [];
//     $data['version'] = '0.1.1';
//     return view('welcome', $data);
// });

Route::get('/di', 'ClientController@di');

Route::get('/facades/db', function () {
    
    return DB::select('SELECT * from table');
});

Route::get('/facades/encrypt', function () {
    
    return Crypt::encrypt('123456789');
});

//eyJpdiI6IjVuV1lWR3JXRlFmdGFHbXljN0Vodnc9PSIsInZhbHVlIjoibEpLQWJSdmgybDBXRHdjNDJadERwM0lZRWlLZnA5d2hcL1wvMHdCNEpCSklFPSIsIm1hYyI6ImE1NDQxZDhiMTAyNjQyNTZkOTZlY2NkZTdmNmIxYThhNjU1OTI2MGI2OTFmYWUxNmRlODk1ZDNiODgxMTY3YzAifQ==

Route::get('/facades/decrypt', function () {
    
    return Crypt::decrypt('eyJpdiI6IjVuV1lWR3JXRlFmdGFHbXljN0Vodnc9PSIsInZhbHVlIjoibEpLQWJSdmgybDBXRHdjNDJadERwM0lZRWlLZnA5d2hcL1wvMHdCNEpCSklFPSIsIm1hYyI6ImE1NDQxZDhiMTAyNjQyNTZkOTZlY2NkZTdmNmIxYThhNjU1OTI2MGI2OTFmYWUxNmRlODk1ZDNiODgxMTY3YzAifQ==');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('room-home');
Route::post('/store/photos', 'ProfileController@uploadPhoto');

// Route::get('/generate/password', function(){ return bcrypt('123456789'); });
