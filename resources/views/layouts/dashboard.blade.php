<!DOCTYPE html>
<html data-whatinput="keyboard" data-whatintent="keyboard" class=" whatinput-types-initial whatinput-types-keyboard">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <title>Profile | Room Rental Club</title>
        <link rel="icon" type="image/x-icon" href="favicon.ico" />
        <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('pickadate/lib/themes/classic.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('pickadate/lib/themes/classic.date.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('pickadate/lib/themes/classic.time.css') }}">
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" media="all">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
   	</head>
   	<body>

   		<nav class="navbar navbar-inverse navbar-fixed-top">
   			<div class="container-fluid">
   				<div class="navbar-header">
   					<button type="button" class="collapsed navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-7" aria-expanded="false">
   						<span class="sr-only">Toggle navigation</span>
   						<span class="icon-bar"></span>
   						<span class="icon-bar"></span>
   						<span class="icon-bar"></span>
   					</button>
   					<a href="{{ route('dashboard') }}" class="navbar-brand">Room Rental Club</a>
   				</div>
   				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-7">
   					<ul class="nav navbar-nav">
   						<li><a href="{{ route('dashboard') }}">@if(Auth::user()->user_type != 'renter') Dashboard @else Viewings @endif</a></li>
   						<li><a href="{{ route('profile') }}">Profile</a></li>
              @if(Auth::user()->user_type != 'renter')
              <li><a href="{{ route('viewing-time') }}">Viewing Time</a></li>
              @endif
              <li><a href="{{ route('schedule') }}">Schedule</a></li>
   						<li class="navbar-right"><a href="{{ route('logout') }}">Logout</a></li>
   					</ul>
   				</div>
   			</div>
   		</nav>
      <div>
        @yield('content')
      </div>
      <script src="{{ asset('pickadate/lib/legacy.js') }}"></script>
      <script src="{{ asset('pickadate/lib/picker.js') }}"></script>
      <script src="{{ asset('pickadate/lib/picker.date.js') }}"></script>
      <script src="{{ asset('pickadate/lib/picker.time.js') }}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
      <script type="text/javascript">
        $(document).ready(function () {
          var url = window.location;
          $('ul.nav a[href="'+ url +'"]').parent().addClass('active');
          $('ul.nav a').filter(function() {
            return this.href == url;
          }).parent().addClass('active');
        });
      </script>
    </body>
</html>