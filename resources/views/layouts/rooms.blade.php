<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Room Rental Club</title>
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="viewport" content="width=device-width">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('pickadate/lib/themes/classic.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('pickadate/lib/themes/classic.date.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('pickadate/lib/themes/classic.time.css') }}">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" media="all">
    <link href="{{ asset('css/home_style.css') }}" rel="stylesheet" type="text/css" media="all">
    <link href="http://fonts.googleapis.com/css?family=Ubuntu:regular,bold" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Vollkorn:regular,italic,bold" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
    
    <!--[if lt IE 9]>
    <script src="js/html5.js"></script>
    <script src="js/IE9.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">

        <div id="headerwrap">
          <header id="mainheader" class="bodywidth clear"> <img src="images/logo.png" alt="" class="logo">
            <hgroup id="websitetitle">
              <h1><span class="bold">RentMe</span>Rooms</h1>
              <h2>Helping roomates find the best room to rent</h2>
            </hgroup>
            <nav>
              <ul>
                <li><a href="Viewings.php">Viewings come up &nbsp;&nbsp;&nbsp;</a></li>
                <i><a href="{{route('find-room')}}">Roomies looking for a home</a></li>

                <li><a href="rooms.php">rooms available</a></li>
                <li> <a href="joinus_roomSeeker.php">Why join us if you are looking for a room </a></li>
                <li><a href="joinus_roomProvider.php">Why join us if you are looking for a roomate or room renter?</a></li>



                <li><a href="utilities.php">Utilities</a></li>


                <li><a href="policies.php">Policies</a></li>

                <li><a href="contact.php">Contact Us</a></li>

                <li><a href="{{ route('feedback') }}">Feedback</a></li>

              </ul>
            </nav>
          </header>
        </div>
        <aside id="introduction" class="bodywidth clear">
          @yield('content')
          
        </aside>
        

        <div id="footerwrap">

          <footer id="mainfooter" class="bodywidth clear">
            <nav class="clear">
        
            </nav>
            <p class="copyright">Website Template By <a target="_blank" href="http://www.tristarwebdesign.co.uk/">Tristar</a> &amp; Modified By <a target="_blank" href="http://www.os-templates.com/">OS Templates</a></p>
          </footer>
        </div>
      </div>
    </div>
    <script src="{{ asset('pickadate/lib/legacy.js') }}"></script>
    <script src="{{ asset('pickadate/lib/picker.js') }}"></script>
    <script src="{{ asset('pickadate/lib/picker.date.js') }}"></script>
    <script src="{{ asset('pickadate/lib/picker.time.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
  </body>
</html>
