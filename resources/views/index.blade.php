@extends('layouts.rooms')

@section('content')

  @if(Session::has('message'))
    <div id="message">
      <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    </div>
  @endif


  <div id="introleft">
        
    <h2> <span class="blue"></span></h2>
   
    <p><a href="viewings.php" class="findoutmore">See Viewings
        more?</a></p>

    <p><a href="possible_roomies.php" class="findoutmore">Lets see possible roomies
        more?</a></p>

    <p><a href="rooms.php" class="findoutmore">Rooms available</a></p>

    <p><a href="joinus_roomSeeker.php" class="findoutmore">Why join if I am looking for a room?</a></p>

    <p><a href="joinus_roomProvider.php" class="findoutmore">Why join if I am renting a room out?</a></p>

    <p><a class="findoutmore" data-toggle="modal" data-target="#house_manager">Log in for house managers</a></p>

    <p><a class="findoutmore" data-toggle="modal" data-target="#prospective_login">Log in for prospective roomies</a></p>

    <p><a href="login_currentroomies.php" class="findoutmore">Log in for current roomies</a></p>

  </div>
  <blockquote id="introquote">
    <p>We are the bnb type website for long term room rentals</p>
    <p class="quotename"> <span class="bold"></span></p>
  </blockquote>

  <!-- Modal -->
  <div id="prospective_login" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Renter's Login</h4>
        </div>
        <div class="modal-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    
                    @foreach ($errors->all() as $error)
                        <span>{{ $error }}</span>
                    @endforeach
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success">
                  
                  <span>{{ Session::get('success') }} you're logged in.</span>
                  
                </div>
            @elseif (Session::has('error'))
                <div class="alert alert-danger">
                  
                  <span>{{ Session::get('error') }}</span>
                  
                </div>
            @endif
          <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
              {{ csrf_field() }}

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                  <div class="col-md-6">
                      <input id="email" type="email" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
                      <input type="hidden" class="form-control" name="user_type" value="renter">

                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password" class="col-md-4 control-label">Password</label>

                  <div class="col-md-6">
                      <input id="password" type="password" class="form-control" name="password" required>

                      @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <!-- <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                      <div class="checkbox">
                          <label>
                              <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                          </label>
                      </div>
                  </div>
              </div> -->

              <div class="form-group">
                  <div class="col-md-8 col-md-offset-4">
                      <button type="submit" class="button success hollow btn btn-primary">
                          Login
                      </button>

                      <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
                          Forgot Your Password?
                      </a> -->
                  </div>
              </div>
          </form>
        </div>
      </div>

    </div>
  </div>

  <div id="house_manager" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">House Manger's Login</h4>
        </div>
        <div class="modal-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    
                    @foreach ($errors->all() as $error)
                        <span>{{ $error }}</span>
                    @endforeach
                </div>
            @endif
            @if(Session::has('house_log_success'))
                <div class="alert alert-success">
                  
                  <span>{{ Session::get('house_log_success') }} you're logged in.</span>
                  
                </div>
            @elseif (Session::has('house_log_error'))
                <div class="alert alert-danger">
                  
                  <span>{{ Session::get('house_log_error') }}</span>
                  
                </div>
            @endif
          <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
              {{ csrf_field() }}

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                  <div class="col-md-6">
                      <input id="email" type="email" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
                      <input type="hidden" class="form-control" name="user_type" value="house-manager">

                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password" class="col-md-4 control-label">Password</label>

                  <div class="col-md-6">
                      <input id="password" type="password" class="form-control" name="password" required>

                      @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <!-- <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                      <div class="checkbox">
                          <label>
                              <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                          </label>
                      </div>
                  </div>
              </div> -->

              <div class="form-group">
                  <div class="col-md-8 col-md-offset-4">
                      <button type="submit" class="button success hollow btn btn-primary">
                          Login
                      </button>

                      <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
                          Forgot Your Password?
                      </a> -->
                  </div>
              </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="modal" data-target="#house_manager_reg">Create Account</button>
        </div>
      </div>

    </div>
  </div>

  <!-- Modal -->
  <div id="house_manager_reg" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">House Manager's Registeration</h4>
        </div>
        <div class="modal-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    
                    @foreach ($errors->all() as $error)
                        <span>{{ $error }}</span>
                    @endforeach
                </div>
            @endif
            @if(Session::has('house_reg_success'))
                <div class="alert alert-success">
                  
                  <span>{{ Session::get('house_reg_success') }} you're logged in.</span>
                  
                </div>
            @endif

          <form method="POST" action="{{ route('house-manager-reg') }}" id="manager_login">
          {{ csrf_field() }}
            <div class="form-group">
              <label for="fname" class="control-label">First Name</label>
              <input type="text" class="form-control" name="fname" value="{{ old('fname') }}">
            </div>
            <div class="form-group">
              <label for="email" class="control-label">Email</label>
              <input type="email" class="form-control" name="email" value="{{ old('email') }}">
            </div>
            <div class="form-group">
              <label for="password" class="control-label">Password</label>
              <input type="password" class="form-control" name="password" value="{{ old('password') }}">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>

    </div>
  </div>

@endsection

<script src="{{ asset('js/jquery.min.js ')}}"></script>
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript">
$("#create_acc").click(function(){
  $('#house_manager').modal('hide');
  $('#house_manager_reg').modal('show');
});

</script>

@if(!empty(Session::get('error_code')) && Session::get('error_code') == 5 || Session::get('success') || Session::get('error'))
<script>
$(function() {
    $('#prospective_login').modal('show');
});
</script>
@endif

@if(!empty(Session::get('house_log_error_code')) && Session::get('house_log_error_code') == 5 || Session::get('house_log_success') || Session::get('house_log_error'))
<script>
$(function() {
    $('#house_manager').modal('show');
});
</script>
@endif

@if(!empty(Session::get('house_reg_error_code')) && Session::get('house_reg_error_code') == 5 || Session::get('house_reg_success'))
<script>
$(function() {
    $('#house_manager_reg').modal('show');
});
</script>
@endif