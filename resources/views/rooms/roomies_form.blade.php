@extends('layouts.rooms')

@section('content')


    <div class="container" style="margin-top:50px;">
        <div class="row">
            <div class="col-md-10 col-sm-10 col-lg-10">
                <h3 class="text-center" style="margin-bottom:20px;"><b>Looking for a Room</b></h3>
                
                @if ($errors->any())
                    <div class="alert alert-danger">
                        
                        @foreach ($errors->all() as $error)
                            <span>{{ $error }}</span>
                        @endforeach
                    </div>
                @endif

                <form method="POST" action="{{ route('prospective_renters') }}">
                {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="fname">First Name</label>
                        <input type="text" class="form-control" name="fname" value="{{ old('fname') }}">
                    </div>
                    <div class="form-group">
                        <label for="lname">Last Name</label>
                        <input type="text" class="form-control" name="lname" value="{{ old('lname') }}">
                    </div>
                    <div class="form-group" >
                        <label for="tel_num">Telephone number</label>
                        <input type="tel" class="form-control" name="tel_num" value="{{ old('tel_num') }}">
                    </div>
                    <div  class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <label for="interested_city">Which rental are you most interested in?</label>
                        <div>
                            <input type="radio" class="form-check-input" name="interested_city" value="centre_street" @if(old('interested_city') == 'centre_street') checked @endif>
                            <label>Centre Street (107 31 ave NE Calgary AB)</label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="interested_city" value="inglewood" @if(old('interested_city') == 'inglewood') checked @endif>
                            <label>Inglewood (10 Saint Monica Ave SE Calgary AB)</label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="interested_city" value="sooke" @if(old('interested_city') == 'sooke') checked @endif>
                            <label>Sooke (7116 Grant Road W Sooke BC)</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="interested_room">Which room are you interested in?</label>
                        <input type="text" class="form-control" name="interested_room" value="{{ old('interested_room') }}">
                    </div>
                    <div class="form-group">
                        <label for="showing_date" class="control-label col-sm-12" style="padding:0;">Which showing date and time did you want to come to?</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control datepicker" name="showing_date" id="showing_date" value="{{ old('showing_date') }}" placeholder="Select Date" />
                        </div>
                        <div class="col-sm-5">
                        
                            <input type="text" class="form-control timepicker" name="showing_time" id="showing_time" value="{{ old('showing_time') }}" placeholder="Select Time" />
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="date">What move in date would you like?</label>
                        <input type="text" class="form-control datepicker" id="move_date" name="move_date" value="{{ old('move_date') }}">
                    </div>

                    <div class="form-group">
                        <label for="job_title">What is your job title, or where are you a student, full or part time?</label>
                        <textarea class="form-control" name="job_title" >{{ old('job_title') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>When is a good time to call?</label>

                        <div>
                            <input type="radio" class="form-check-input" name="good_time" value="weekeday evenings" @if(old('good_time') == 'weekeday evenings') checked @endif>
                            <label>weekday evenings</label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="good_time" value="weekday day time" @if(old('good_time') == 'weekday day time') checked @endif>
                            <label>weekday day time</label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="good_time" value="weekend day time" @if(old('good_time') == 'weekend day time') checked @endif>
                            <label>weekend day time</label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="good_time" value="weekend early evening" @if(old('good_time') == 'weekend early evening') checked @endif>
                            <label>weekend early evening</label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="good_time" value="other, I will email you a time" @if(old('good_time') == 'other, I will email you a time') checked @endif>
                            <label>other, I will email you a time</label>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="roommates">how many people in your room</label>
                        <select name="roommates" class="form-control"  >
                            <option value="just me" @if(old('roommates') == 'just me') selected @endif>just me</option>
                            <option value="me and my partner" @if(old('roommates') == 'me and my partner') selected @endif>me and my partner</option>
                            <option value="other" @if(old('roommates') == 'other') selected @endif>other</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Do you have landlord reference</label>
                        
                        <div>
                            <input type="radio" class="form-check-input" name="refer" value="yes, and I can verify them with leases/receipts"  @if(old('refer') == 'yes, and I can verify them with leases/receipts') checked @endif>
                            <label>yes, and I can verify them with leases/receipts</label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="refer" value="Yes, I have some"  @if(old('refer') == 'Yes, I have some') checked @endif>
                            <label>Yes, I have some</label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="refer" value="I probably have some"  @if(old('refer') == 'I probably have some') checked @endif>
                            <label>I probably have some</label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="refer" value="I am new to renting here" @if(old('refer') == 'I am new to renting here') checked @endif>
                            <label>I am new to renting here</label>
                        </div>

                    </div>
                    <div class="form-group">
                        <label>do you smoke</label>
                        
                        <div>
                            <input type="radio" class="form-check-input" name="smoke" value="yes"  @if(old('smoke') == 'yes') checked @endif>
                            <label>yes</label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="smoke" value="no" @if(old('smoke') == 'no') checked @endif>
                            <label>no</label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="smoke" value="occasionaly" @if(old('smoke') == 'occasionaly') checked @endif>
                            <label>occasionaly</label>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="whatsapp">do you have whatsap?</label>
                        <select name="whatsapp" class="form-control">
                            <option value="yes" @if(old('whatsapp') == 'yes') selected @endif>yes</option>
                            <option value="no" @if(old('whatsapp') == 'no') checked @endif>no</option>
                            <option value="unfamilar with it" @if(old('whatsapp') == 'unfamiliar with it') checked @endif>unfamilar with it</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Do you have a pet? If so, what is he/she?</label>
                        <textarea class="form-control" name="pet_info" >{{ old('pet_info') }}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label>What  price range are you looking at for a room ?  (Please include in this price the cost of utilities and wifi)</label>
                        <input type="text" class="form-control" name="price_range" value="{{ old('price_range') }}">
                        
                    </div>
                    <div class="form-group">
                        <label>Any questions for us?</label>
                        <textarea class="form-control" name="query_ques" value="{{ old('query_ques') }}">
                        </textarea>
                    </div>
                
                    <!-- <div class="form-group">
                        <label>Comment</label>
                        <input type="text" name="comments" class="form-control" value="{{ old('comments') }}">
                    </div> -->
                    <div class="wpforms-submit-container" >
                        <!-- <input type="hidden" name="wpforms[id]" value="41">
                        <input type="hidden" name="wpforms[author]" value="1">
                        <input type="hidden" name="wpforms[post_id]" value="9"> -->
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

<script src="{{ asset('js/jquery.min.js ')}}"></script>
<script type="text/javascript">

    // $(function() {
    //     $( "#move_date").datepicker();
    // });

    $(function() {
        $('.datepicker').pickadate();
        $('.timepicker').pickatime();
    });

</script>