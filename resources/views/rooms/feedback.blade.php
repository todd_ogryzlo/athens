@extends('layouts.rooms')

@section('content')


    <div class="container" style="margin-top:50px;">
        <div class="row">
            <div class="col-md-10 col-sm-10 col-lg-10">
                <h3 class="text-center" style="margin-bottom:20px;"><b>Feedback</b></h3>
                
                @if ($errors->any())
                    <div class="alert alert-danger">
                        
                        @foreach ($errors->all() as $error)
                            <span>{{ $error }}</span>
                        @endforeach
                    </div>
                @endif

                <form method="POST" action="{{ action('FeedbackController@store') }}">
                {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="fname">First Name</label>
                        <input type="text" class="form-control" name="fname" value="{{ old('fname') }}">
                    </div>
                    <div class="form-group">
                        <label for="bathroom">Bathroom score</label>
                        <div>
                            <input type="radio" class="form-check-input" name="bathroom" value="1" @if(old('bathroom') == '1') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="bathroom" value="2" @if(old('bathroom') == '2') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="bathroom" value="3" @if(old('bathroom') == '3') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="bathroom" value="3" @if(old('bathroom') == '4') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="bathroom" value="3" @if(old('bathroom') == '5') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="kitchen">Kitchen Score</label>
                        <div>
                            <input type="radio" class="form-check-input" name="kitchen" value="1" @if(old('kitchen') == '1') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="kitchen" value="2" @if(old('kitchen') == '2') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="kitchen" value="3" @if(old('kitchen') == '3') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="kitchen" value="3" @if(old('kitchen') == '4') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="kitchen" value="3" @if(old('kitchen') == '5') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="living_room">Living room cleaninliness</label>
                        <div>
                            <input type="radio" class="form-check-input" name="living_room" value="1" @if(old('living_room') == '1') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="living_room" value="2" @if(old('living_room') == '2') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="living_room" value="3" @if(old('living_room') == '3') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="living_room" value="3" @if(old('living_room') == '4') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="living_room" value="3" @if(old('living_room') == '5') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lawn_cutting">Lawn cutting of front yard and side walk beside centre street</label>
                        <div>
                            <input type="radio" class="form-check-input" name="lawn_cutting" value="1" @if(old('lawn_cutting') == '1') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="lawn_cutting" value="2" @if(old('lawn_cutting') == '2') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="lawn_cutting" value="3" @if(old('lawn_cutting') == '3') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="lawn_cutting" value="3" @if(old('lawn_cutting') == '4') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="lawn_cutting" value="3" @if(old('lawn_cutting') == '5') checked @endif>
                            <label><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cleaning_comment">Any cleaning comments you wish to share?</label>
                        <input type="text" class="form-control" name="cleaning_comment" value="{{ old('cleaning_comment') }}">
                    </div>
                    <div class="form-group">
                        <label for="toilet_cleaning">We have enought toilet paper and cleaning supplies</label>
                        <div>
                            <input type="radio" class="form-check-input" name="toilet_cleaning" value="strongly_disagree" @if(old('toilet_cleaning') == 'strongly_disagree') checked @endif>
                            <label>strongly disagree, we need some things!</label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="toilet_cleaning" value="disagree" @if(old('toilet_cleaning') == 'disagree') checked @endif>
                            <label>disagree</label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="toilet_cleaning" value="neutral" @if(old('toilet_cleaning') == 'neutral') checked @endif>
                            <label>neutral</label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="toilet_cleaning" value="agree" @if(old('toilet_cleaning') == 'agree') checked @endif>
                            <label>agree, yes we have enough supplies</label>
                        </div>
                        <div>
                            <input type="radio" class="form-check-input" name="toilet_cleaning" value="strongly_agree" @if(old('toilet_cleaning') == 'strongly_agree') checked @endif>
                            <label>strongly agree</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="needed[]">I think we need...</label>
                        <div>
                            <input type="checkbox" class="form-check-input" name="needed[]" value="mr clean" @if(old('needed') == 'mr clean') checked @endif>
                            <label>we need mr clean</label>
                        </div>
                        <div>
                            <input type="checkbox" class="form-check-input" name="needed[]" value="dish washing sponges" @if(old('needed') == 'dish washing sponges') checked @endif>
                            <label>we need dish washing sponges</label>
                        </div>
                        <div>
                            <input type="checkbox" class="form-check-input" name="needed[]" value="wash clothes" @if(old('needed') == 'wash clothes') checked @endif>
                            <label>we need wash clothes for cleaning</label>
                        </div>
                        <div>
                            <input type="checkbox" class="form-check-input" name="needed[]" value="garbage bags" @if(old('needed') == 'garbage bags') checked @endif>
                            <label>we need garbage bags</label>
                        </div>
                        <div>
                            <input type="checkbox" class="form-check-input" name="needed[]" value="dish soap" @if(old('needed') == 'dish soap') checked @endif>
                            <label>we need dish soap</label>
                        </div>
                        <div>
                            <input type="checkbox" class="form-check-input" name="needed[]" value="rubber gloves" @if(old('needed') == 'rubber gloves') checked @endif>
                            <label>we need rubber gloves</label>
                        </div>
                        <div>
                            <input type="checkbox" class="form-check-input" name="needed[]" value="toilet wash cloth" @if(old('needed') == 'toilet wash cloth') checked @endif>
                            <label>we need a wash cloth specifically for the toilet</label>
                        </div>
                        <div>
                            <input type="checkbox" class="form-check-input" name="needed[]" value="toilet paper" @if(old('needed') == 'toilet paper') checked @endif>
                            <label>we need toilet paper</label>
                        </div>
                        <div>
                            <input type="checkbox" class="form-check-input" name="needed[]" value="windex" @if(old('needed') == 'windex') checked @endif>
                            <label>we need windex</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="supplies_needed">Please list any supplies which we need</label>
                        <input type="text" class="form-control" name="supplies_needed" value="{{ old('supplies_needed') }}">
                    </div>
                    <div class="form-group">
                        <label for="suggestion">What do you need? Suggestions? Criticisms? You have got our attenion, just fill in the blank!</label>
                        <input type="text" class="form-control" name="suggestion" value="{{ old('suggestion') }}">
                    </div>

                    <div class="wpforms-submit-container" >
                        <!-- <input type="hidden" name="wpforms[id]" value="41">
                        <input type="hidden" name="wpforms[author]" value="1">
                        <input type="hidden" name="wpforms[post_id]" value="9"> -->
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
