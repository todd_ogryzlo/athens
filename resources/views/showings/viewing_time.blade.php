@extends('layouts.dashboard')

@section('content')
@php $renters = array() @endphp
	<div class="container" id="tabs">
   		<div class="row">
   			<div class="col-lg-12 col-md-12 col-sm-10 col-xs-10">
   				<div>
	   				<h4><b>Viewing Time</b></h4>
	   			</div>
	   			<div class="text-right">
	   				<a href="{{ route('schedule') }}" class="btn btn-danger btn-md">Cancel</a>
	   			</div>
	   			<form class="form-horizontal" role="form"  method="POST" 
	   			@if(isset($schedule))
	   				action="{{ route('update-viewing') }}"
	   			@else
	   				action="{{ route('store-viewing') }}"
	   			@endif

	   			id="create_showings">
	   				{{ csrf_field() }}

	   				<div class="form-group">

	                    <label for="user" class="col-sm-3 control-label">Interested User :</label>
	                    <div class="col-sm-6">
		                    <select class="form-control" name="user">
	                        	<option value="">Choose</option>
	                        	@if(isset($schedule))
				            		@foreach($schedule->renters as $schedule_renter)
		                        		@php
		                        			array_push($renters,$schedule_renter->id)
		                        		@endphp
	                        		@endforeach
	                        	@endif
	                        	@if(isset($properties))
		                        	@foreach($properties as $value)
		                        		@foreach($value->renters as $renter)
		                        			<option value="{{$renter->id}}" @if(old('user') == "{{ $renter->id}}") selected @elseif(in_array($renter->id,$renters)) selected @endif>{{ $renter->name }}</option>
		                        		@endforeach
		                        	@endforeach
	                        	@endif
	                        </select>
	                        @if(isset($schedule) && isset($renters) && !empty($renters))
			   					<input type="hidden" name="previous_user" value="{{$renters[0]}}">
			   				@endif
		                </div>
	                </div>
	   				<div class="form-group">
	                    <label for="address" class="col-sm-3 control-label">Address :</label>
	                    <div class="col-sm-6">
	                    	@if(isset($properties))
	                    		@foreach($properties as $value)
		                    		<input type="text" class="form-control" name="address" value="@if(isset($schedule->date)){{ $schedule->address }} @else {{ $value->house_address}} @endif">
	                    		@endforeach
	                    	@else
	                    		<input type="text" class="form-control" name="address" value="@if(isset($schedule->date)){{ $schedule->address }} @else {{ old('address') }} @endif">
	                    	@endif
		                </div>
	                </div>
	                <div class="form-group">
	                    <label for="rooms" class="col-sm-3 control-label">For Rooms :</label>
	                    <div class="col-sm-6">
		                    <input type="text" class="form-control" name="rooms" value="@if(isset($schedule->rooms)){{ $schedule->rooms }} @else {{ old('rooms')}} @endif">
		                </div>
	                </div>
	   				<div class="form-group">
	   					<label class="col-sm-3 control-label">Viewing Date :</label>
	   					<div class="col-sm-6">
				            <div class='input-group date' id='date'>
				            	@if(isset($schedule))
				            		<input type="hidden" name="schedule_id" value="{{$schedule->id}}">
				                @endif
				                <input type='text' class="form-control datepicker" name="date" value="@if(isset($schedule->date)){{ $schedule->date }} @else {{ old('date')}}@endif"/>
				                <span class="input-group-addon">
				                    <span class="glyphicon glyphicon-calendar">
				                    </span>
				                </span>
				            </div>
				        </div>
			        </div>
			        <div class="form-group">
	   					<label class="col-sm-2 control-label">From :</label>
	   					<div class="col-sm-3">
				            <div class='input-group date'>
				                <input type='text' class="form-control timepicker" name="time_from" value="@if(isset($schedule->date)){{ $schedule->time_from }} @else {{ old('time_from')}}@endif"/>
				                <span class="input-group-addon">
				                    <span class="glyphicon glyphicon-time">
				                    </span>
				                </span>
				            </div>
				        </div>
				        <label class="col-sm-2 control-label">To :</label>
	   					<div class="col-sm-3">
				            <div class='input-group date'>
				                <input type='text' class="form-control timepicker" name="time_to" value="@if(isset($schedule->time_to)) {{ $schedule->time_to }} @else {{ old('time_to')}}@endif"/>
				                <span class="input-group-addon">
				                    <span class="glyphicon glyphicon-time">
				                    </span>
				                </span>
				            </div>
				        </div>
			        </div>
			        <div class="text-right col-lg-10 col-md-10 col-sm-8 col-xs-8">
			        	@if(isset($schedule))
			        		<button type="submit" class="btn btn-success btn-md">Update</button>
			        	@else
							<button type="submit" class="btn btn-success btn-md">Submit</button>
			        	@endif
					</div>

	   			</form>
	   			
	   		</div>
   		</div>
   	</div>



@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        
<script type="text/javascript">
	$(function() {
        $('.datepicker').pickadate();
        $('.timepicker').pickatime();
    });
</script>