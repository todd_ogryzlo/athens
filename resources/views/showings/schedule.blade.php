@extends('layouts.dashboard')

@section('content')

	<div class="container" id="schedule">
   		<div class="row">
   			<div class="col-lg-12 col-md-12 col-sm-10 col-xs-10">
   				<h3><b>Schedule</b></h3>
   				<div class="col-lg-10 col-md-10 col-lg-offset-1 col-md-offset-1">
	   				<table class="table table-hover table-responsive table-striped table-hover">
	   					<thead>
	   						<tr>
	   							<th>Date</th> 
	   							<th>From</th> 
	   							<th>To</th>
	   							@if($user_type != 'renter')
	   							<th>Interested</th>
	   							<th>Address</th>
	   							<th>Rooms</th>
	   							<th>Action</th>
	   							@else
	   							<th>Property Owner</th>
	   							<th>Price</th>
	   							<th>Rooms</th>
	   							<th>Address</th>
	   							@endif 
	   						</tr> 
	   					</thead> 
	   					<tbody>
	   						@if(isset($schedule))
	   							@foreach($schedule as $key => $value)
	   								@if($user_type != 'renter')
		   								@foreach($value->renters as $r => $renter) 
		   									<tr> 
					   							<td>{{ $value->date}}</td> 
					   							<td>{{ $value->time_from}}</td> 
					   							<td>{{ $value->time_to}}</td>
					   							<td>{{ $renter->name}}</td>
					   							<td>{{ $value->address}}</td>
					   							<td>{{ $value->rooms}}</td>
					   							<td>
					   								<a class="btn btn-success" href="{{ action('ShowingsController@editViewingTime',[$value->id])}}">Edit</a>
					   								<button class="btn btn-danger" data-toggle="modal" data-target="#delete_schedule{{$value->id}}">Delete</button>
					   							</td>
					   							
	 
					   							<div id="delete_schedule{{$value->id}}" class="modal fade" role="dialog">
												    <div class="modal-dialog">

												      <!-- Modal content-->
												      <div class="modal-content">
												        <div class="modal-header">
												          <button type="button" class="close" data-dismiss="modal">&times;</button>
												          <h4 class="modal-title">Delete</h4>
												        </div>
												        <div class="modal-body">
												             
												            Do you want to delete this schedule?

												        </div>
												        <div class="modal-footer">
													        <form method="post" action="{{ action('ShowingsController@deleteSchedule',[$value->id])}}">
													        	{{ csrf_field() }}
													            <a class="btn btn-default" data-dismiss="modal">No</a>
													        	<button type="submit" class="btn btn-danger">Yes</a>
													        	<input type="hidden" name="renter" value="{{ $renter->id }}">  
													        </form>
												        </div>
												      </div>

												    </div>
											  	</div>
											</tr>		   						
					   					@endforeach
					   				@else
		   								@foreach($value->manager as $m => $manager)
		   									@php $property = $manager->with('property')->get() @endphp
			   								@foreach($property as $details)
			   									@if(count($details->property) > 0)
			   										@foreach($details->property as $prop)
			   											@if($manager['username'] == $prop->email)
				   											<tr>
				   												<td>{{ $value->date}}</td> 
									   							<td>{{ $value->time_from}}</td> 
									   							<td>{{ $value->time_to}}</td>
				   												<td class="text-capitalize">{{ $prop->first_name }}</td>
				   												<td>{{ $prop->price }}</td>
									   							<td>{{ $value->rooms}}</td>
									   							<td>{{ $value->address}}</td>
									   						</tr>
			   											@endif
				   									@endforeach
				   								@endif
				   							@endforeach
			   							@endforeach	
				   					@endif
		   						@endforeach
	   						@endif 
	   					</tbody>
	   				</table>
   				</div>
   			</div>
   		</div>
   	</div>

   	
@endsection