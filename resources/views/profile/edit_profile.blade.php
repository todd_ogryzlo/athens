@extends('layouts.dashboard')

@section('content')

	<div class="container" id="tabs">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-10 col-xs-10">
				<ul class="nav nav-tabs">
			   <li class="active"><a href="#edit_profile" data-toggle="tab">Edit Profile</a></li>
			   @if(Auth::user()->user_type != 'renter')
			   <li><a href="#photos" data-toggle="tab">Photos & Details</a></li>
			   <li><a href="#availability" data-toggle="tab">Availability</a></li>
			   @else
			   <li><a href="#requirement" data-toggle="tab">Requirement</a></li>
			   @endif
			</ul>
			@if($errors->any())
                <div class="alert alert-danger">
                    
                    @foreach ($errors->all() as $error)
                        <span>{{ $error }}</span>
                    @endforeach
                </div>
            @endif
			<form class="form-horizontal" id="tab_form"  enctype="multipart/form-data" method="POST" action="{{ action('ProfileController@store') }}">
				{{ csrf_field() }}
				<div class="tab-content">
					<div class="tab-pane active" id="edit_profile">
					   	<div class="row">
					   		@if(isset($user))
							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
								<div class="form-group">
									<label class="col-sm-2 control-label">Upload</label>
									<div class="col-sm-4">
										@if(isset($details))
					                    <div id="profile-container">
										   <image id="profileImage" src="{{ asset('uploads/'.$details->photo) }}" />
										</div>
										@endif
										<input type="file" name="profile_photo" class="form-control" value="{{ old('profile_photo')}}">
									</div>
				                </div>
								<div class="form-group">
				                    <label for="fname" class="col-sm-2 control-label">First Name</label>
				                    <div class="col-sm-8">
					                    <input type="text" class="form-control" name="fname" value="{{ old('fname', $user->name) }}" @if($user->name) readonly @endif>
					                </div>
				                </div>
				                <div class="form-group">
			                        <label for="lname" class="col-sm-2 control-label">Last Name</label>
			                        <div class="col-sm-8">
			                        	<input type="text" class="form-control" name="lname" value="@if(isset($details)) {{ $details->last_name }} @else {{ old('lname')}}  @endif">
			                    	</div>
			                    </div>
			                    <div class="form-group">
			                        <label for="gender" class="col-sm-2 control-label">Gender</label>
			                        <div class="col-sm-4">
				                        <select class="form-control" name="gender">
				                        	<option value="">Choose</option>
				                        	<option value="male" @if(old('gender') == 'male') selected @elseif(isset($details) && $details->gender == 'male') selected @endif>Male</option>
				                        	<option value="female" @if(old('gender') == 'female') selected @elseif(isset($details) && $details->gender == 'female') selected @endif>Female</option>
				                        	<option value="other" @if(old('gender') == 'other') selected @elseif(isset($details) && $details->gender == 'other') selected @endif>Other</option>
				                        </select>
			                    	</div>
			                    </div>
			                    <div class="form-group">
			                    	<label for="birth_date" class="col-sm-2 control-label">Birth Date</label>
			                    	<div class="col-sm-8">
			                    		<div class='input-group date'>
				                    		<input type='text' class="form-control datepicker" name="birth_date" value="@if(isset($details)) {{ $details->birth_date }} @else {{ old('birth_date') }} @endif"/>
									        <span class="input-group-addon">
									            <span class="glyphicon glyphicon-calendar">
									            </span>
									        </span>
									    </div>
			                    	</div>
			                    </div>
			                    <div class="form-group">
			                        <label for="email" class="col-sm-2 control-label">Email</label>
			                        <div class="col-sm-8">
			                        	<input type="email" class="form-control" name="email" value="{{ old('email', $user->username) }}" readonly>
			                        </div>
			                    </div>
			                    <div class="form-group" >
			                        <label for="mob_num" class="col-sm-2 control-label">Mobile number</label>
			                        <div class="col-sm-8">
			                        	<input type="text" class="form-control" name="mob_num" value="@if(isset($details)) {{$details->mobile }} @else {{ old('mob_num') }} @endif">
			                        </div>
			                    </div>
			                    <div class="form-group">
			                    	<label for="address" class="col-sm-2 control-label">Address</label>
			                    	<div class="col-sm-8">
			                    		<input type="text" class="form-control" id="address" name="address" value="@if(isset($details)) {{ $details->address }} @else {{ old('address') }} @endif">
			                    	</div>
			                    </div>
			                    <div class="form-group">
				   					<label for="district" class="col-sm-2 control-label">City</label>
				   					<div class="col-sm-3">
							            <input type='text' class="form-control" name="district" value="@if(isset($details)) {{ $details->district }} @else {{ old('district') }} @endif"/>
							        </div>
							        <label for="state" class="col-sm-2 control-label">State</label>
				   					<div class="col-sm-3">
							            <input type='text' class="form-control" name="state" value="@if(isset($details)) {{ $details->state }} @else {{ old('state') }}@endif"/>
							        </div>
						        </div>
						        <div class="form-group">
			                        <label for="zipcode" class="col-sm-2 control-label">Zipcode</label>
			                        <div class="col-sm-5">
			                        	<input type="text" class="form-control" name="zipcode" value="@if(isset($details)) {{ $details->zipcode }} @else {{ old('zipcode') }} @endif">
			                        </div>
			                    </div>
							</div>
							@endif
						</div>
					</div>
					@if(Auth::user()->user_type != 'renter')
				   	<div class="tab-pane" id="photos">
				   		<div class="form-group">
	                    	<label for="house_address" class="col-sm-2 control-label">House Address</label>
	                    	<div class="col-sm-8">
	                    		<input type="text" class="form-control" name="house_address" value="@if(isset($details)) {{ $details->house_address }} @else {{ old('house_address') }} @endif">
	                    	</div>
	                    </div>
	                    <div class="form-group">
							<label class="col-sm-2 control-label">House Image</label>
							<div class="col-sm-4">
								@if(isset($details) && $details->house_img)
			                    <div id="profile-container">
								   <image id="profileImage" src="{{ asset('uploads/'.$details->house_img) }}" />
								</div>
								@endif
								<input type="file" name="house_img" class="form-control" value="{{ old('house_img')}}">
							</div>
		                </div>
		                <div class="form-group">
	                        <label for="price" class="col-sm-2 control-label">Price</label>
	                        <div class="col-sm-5">
	                        	<input type="text" class="form-control" name="price" value="@if(isset($details)) {{ $details->price }} @else {{ old('price') }} @endif">
	                        </div>
	                    </div>	
				   	</div>
				   	<div class="tab-pane" id="availability">
				   		<div class="form-group">
		                    <label for="available_from" class="col-sm-2 control-label">Available From :</label>
		                    <div class="col-sm-8">
	                    		<div class='input-group date'>
		                    		<input type='text' class="form-control datepicker" name="available_from" value="@if(isset($details)) {{ $details->available_from }} @else {{ old('available_from') }} @endif"/>
							        <span class="input-group-addon">
							            <span class="glyphicon glyphicon-calendar">
							            </span>
							        </span>
							    </div>
	                    	</div>
		                </div>
		                
				   	</div>
				   	@else
				   	<div class="tab-pane" id="requirement">
				   		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
				   			<div class="form-group">
			                    <label for="num_rooms" class="col-sm-2 control-label">Number of Rooms</label>
			                    <div class="col-sm-8">
				                    <input type="text" class="form-control" name="num_rooms" value="@if(isset($details)) {{ $details->rooms }} @else{{ old('num_rooms')}}  @endif">
				                </div>
			                </div>
			                <div class="form-group">
			                    <label for="house_type" class="col-sm-2 control-label">House Type</label>
			                    <div class="col-sm-5">
				                    <select class="form-control" name="house_type">
			                        	<option value="">Choose</option>
			                        	<option value="flats_apartments" @if(old('house_type') == 'flats_apartments') selected @elseif(isset($details) && $details->house_type == 'flats_apartments') selected @endif>Flats and Apartments</option>
			                        	<option value="bungalows" @if(old('house_type') == 'bungalows') selected @elseif(isset($details) && $details->house_type == 'bungalows') selected @endif>Bungalows</option>
			                        	<option value="farmhouses" @if(old('house_type') == 'farmhouses') selected @elseif(isset($details) && $details->house_type == 'farmhouses') selected @endif>Farmhouses</option>
			                        	<option value="villas" @if(old('house_type') == 'villas') selected @elseif(isset($details) && $details->house_type == 'villas') selected @endif>Villas</option>
			                        </select>
				                </div>
			                </div>
			                <div class="form-group">
			                    <label for="required_for" class="col-sm-2 control-label">Required For</label>
			                    <div class="col-sm-5">
				                    <select class="form-control" name="required_for">
			                        	<option value="">Choose</option>
			                        	<option value="girls" @if(old('required_for') == 'girls') selected @elseif(isset($details) && $details->required == 'girls') selected @endif>Girls</option>
			                        	<option value="boys" @if(old('required_for') == 'boys') selected @elseif(isset($details) && $details->required == 'boys') selected @endif>Boys</option>
			                        	<option value="girls_boys" @if(old('required_for') == 'girls_boys') selected @elseif(isset($details) && $details->required == 'girls_boys') selected @endif>Girls and Boys</option>
			                        	<option value="family" @if(old('required_for') == 'family') selected @elseif(isset($details) && $details->required == 'family') selected @endif>Family</option>
			                        </select>
				                </div>
			                </div>
				   		</div>
				   	</div>
				   	@endif
				   		
				</div>
				<div class="text-right col-lg-10 col-md-10 col-sm-10">
					<button type="submit" class="btn btn-success btn-md">Save</button>
				</div>
			</form>
				
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
	        $('.datepicker').pickadate({
	            labelMonthSelect: 'Pick a month from the dropdown',
	            labelYearSelect: 'Pick a year from the dropdown',
	            selectMonths: true,
	            selectYears: 200,
	            min: new Date(1910,1,1),
          		max: new Date(2080,11,31)
	        });
	    });
		$("#upload_photo").change(function(){
			
			var files = $(this)[0].files;
			var array = [];
			console.log(files);
			for (var i = 0; i < files.length; i++)
			{	
				array.push(files[i].name);
				console.log(files[i].name);
				// Storage::disk('public')->put('filename', $file_content);
				$("#uploaded").append('<div><image src="'+ files[i].name +'" name="'+ i +'"></div>')
			}
			console.log(array);
			$.ajax({
				url: '/store/photos',
				type: 'post',
				data: {'photos' : array } ,
				success: function(result){
					console.log(result);
				}
			});
		});

		$("#profileImage").click(function(e) {
			$("#imageUpload").click();
        });

        function fasterPreview( uploader ) {
            if ( uploader.files && uploader.files[0] ){
                  $('#profileImage').attr('src', 
                     window.URL.createObjectURL(uploader.files[0]) );
            }
        }


      	$("#imageUpload").change(function(){
    		fasterPreview( this );
    	});

        
	</script>
@endsection   		
   		
   	