@extends('layouts.dashboard')

@section('content')

	<div class="container" id="tabs">
		<div class="row">
			<div>
				<h3>Introducing Room Rental Club</h3>
				<h5>A new selection of homes verified for quality and comfort</h5>
				<div class="col-lg-12 col-md-12 col-sm-12">
					<img src="{{ asset('images/home.jpg')}}" id="cover">
				</div>
			</div>
		</div>
		<div id="properties">
			<div class="row">
				@if(isset($properties))
					@foreach($properties as $key => $details)
						@if(Auth::user()->user_type != 'renter')
							@if(Auth::user()->username == $details->email)
								<div class="col-sm-6 col-md-4">
								    <div class="thumbnail">
								    	<img src="{{ asset('uploads/'.$details->house_img) }}" />
								    	<div class="caption">
								        	<!-- <h3>Thumbnail label</h3> -->
									        <p class="text-capitalize"><strong>Owner: </strong>{{ $details->first_name}} {{ $details->last_name }}</p>
									        <p><strong>Address: </strong>{{ $details->house_address}}</p>
									        <p><strong>Price: </strong>{{ $details->price}}</p>
									        <p><strong>Available from: </strong>{{ $details->available_from}}</p>
									        <p><strong>Mobile: </strong>{{ $details->mobile}}</p>
									        <p><strong>Mail: </strong>{{ $details->email}}</p>
									        <p>
									        	<form method="POST" action="{{ action('DashboardController@interestedRenters') }}">
									        		{{ csrf_field() }}
									        		<input type="hidden" name="renter" value="{{ Auth::user()->id }}">
									        		<input type="hidden" name="property" value="{{ $details->id}}">
									        		<input type="hidden" name="property_mail" value="{{ $details->email}}">
										        	<!-- <button type="submit" class="btn btn-primary" @if(Auth::user()->user_type != 'renter' || (isset($interested_renter) && count($interested_renter) > 0 ) && (in_array($details->id,$interested_renter))) disabled @endif>Viewing Sign up</button>
										        	<a href="#" class="btn btn-default" role="button">Button</a> -->
									        	</form>
									        </p>
								    	</div>
									</div>
						  		</div>
							@endif
						@else

						  	<div class="col-sm-6 col-md-4">
							    <div class="thumbnail">
							    	<img src="{{ asset('uploads/'.$details->house_img) }}" />
							    	<div class="caption">
							        	<!-- <h3>Thumbnail label</h3> -->
								        <p class="text-capitalize"><strong>Owner: </strong>{{ $details->first_name}} {{ $details->last_name }}</p>
								        <p><strong>Address: </strong>{{ $details->house_address}}</p>
								        <p><strong>Price: </strong>{{ $details->price}}</p>
								        <p><strong>Available from: </strong>{{ $details->available_from}}</p>
								        <p><strong>Mobile: </strong>{{ $details->mobile}}</p>
								        <p><strong>Mail: </strong>{{ $details->email}}</p>
								        <p>
								        	<form method="POST" action="{{ action('DashboardController@interestedRenters') }}">
								        		{{ csrf_field() }}
								        		<input type="hidden" name="renter" value="{{ Auth::user()->id }}">
								        		<input type="hidden" name="property" value="{{ $details->id}}">
								        		<input type="hidden" name="property_mail" value="{{ $details->email}}">
									        	<button type="submit" class="btn btn-primary" @if(Auth::user()->user_type != 'renter' || (isset($interested_renter) && count($interested_renter) > 0 ) && (in_array($details->id,$interested_renter))) disabled @endif>Viewing Sign up</button>
									        	<!-- <a href="#" class="btn btn-default" role="button">Button</a> -->
								        	</form>
								        </p>
							    	</div>
								</div>
					  		</div>
						@endif
					@endforeach
				@endif
			</div>
		</div>
	</div>

@endsection