<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Login Credentials for <i>Prospective Renters</i></h2>

<div>
	<h5>You can use these credentials to login as a prospective renter.</h5>
	<p><b>Username :</b> {!! $username !!}</p>
	<p><b>Password :</b> {!! $password !!}</p>

</div>

</body>
</html>
