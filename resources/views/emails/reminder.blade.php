<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>{!! $status!!} Schedule</h2>

<div>
   So sorry, but your viewing on {!! $date !!} from {!! $time_from !!} to {!! $time_to !!} is {!! $status !!}.
</div>

</body>
</html>