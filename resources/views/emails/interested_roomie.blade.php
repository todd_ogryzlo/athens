<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Interested in your Property</h2>

<div>
	I can come to see the room on {!! $date !!} at {!! $time!!}.
	You can contact me on {!! $time_to_call !!}.
</div>

</body>
</html>