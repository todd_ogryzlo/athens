<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Viewing Schedule</h2>

<div>
   Your viewing on {!! $date !!} from {!! $time_from !!} to {!! $time_to !!} is Scheduled.
</div>

</body>
</html>