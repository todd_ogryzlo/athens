<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Profile;
use App\User;
use Mail;
use App\ProspectiveRenter;

class DashboardController extends Controller
{	
	public function index()
	{
		$house_manager = User::where('user_type','house-manager')->get();
		$manager_email = array();
		foreach ($house_manager as $key => $value) {
			array_push($manager_email, $value->username);
		}
		$properties = Profile::whereIn('email',$manager_email)->get();
		$interested_renter = Auth::user()->property->pluck('id')->toArray();
		// print_r($interested_renter);exit;
		return view('dashboard.show',compact('properties','interested_renter'));
	}

	public function interestedRenters(Request $request)
	{
		$property = Profile::find($request->property);
		$property->renters()->attach($request->renter);

		$prospective_renter = ProspectiveRenter::where("email", Auth::user()->username)->first();

		$data = array(
	        'date' => date('l jS \of F Y', strtotime($prospective_renter->showing_date)),
	        'time' => $prospective_renter->showing_time,
	        'time_to_call' => $prospective_renter->good_time,
	    );

		$user_email = Auth::user()->username;
		$owner_mail = $request->property_mail;
	    if($user_email){

		    Mail::send('emails.interested_roomie', $data, function ($message) use ($user_email, $owner_mail) {

		        $message->from($user_email, 'Roomie');

		        $message->to($owner_mail)->subject('Interested Roomie');

		    });
		}

		return redirect()->route('dashboard');
	}
}