<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\ViewingSchedule;
use App\User;
use Mail;
use App\Profile;
use DB;

class ShowingsController extends Controller
{

	public function createViewingTime()
	{
		$user_type = Auth::user()->user_type;
		if($user_type == 'renter'){

			return redirect()->route('profile');

		}else{

			$properties = Profile::where('email',Auth::user()->username)->with('renters')->get();
			return view('showings.viewing_time',compact('properties'));
		}
	}

	public function storeViewingTime(Request $request)
	{	
		$schedule = new ViewingSchedule();
		
		$schedule->address = $request->address;
		$schedule->rooms = $request->rooms;
		$schedule->date = $request->date;
		$schedule->time_from = $request->time_from;
		$schedule->time_to = $request->time_to;
		$schedule->save();

		Auth::user()->schedule()->attach($schedule->id);
		$schedule->renters()->attach($request->user);

		$data = array(
	        'date' => date('l jS \of F Y', strtotime($request->date)),
	        'time_from' => $request->time_from,
	        'time_to' => $request->time_to,
	        'status' => "Created",
	    );

		$user = User::find($request->user);
		$own_email = Auth::user()->username;

		if($user){

		    Mail::send('emails.schedule', $data, function ($message) use ($user, $own_email) {

		        $message->from($own_email, 'Room Rental Club');

		        $message->to($user->username)->subject('Schedule');

		    });
		}

		return redirect()->route('schedule');
	}

	public function schedule()
	{
		$user_type = Auth::user()->user_type;
		
		if($user_type != 'renter'){

			$schedule = Auth::user()->schedule()->with('renters')->get();
		}else{

			$schedule = Auth::user()->renters_schedule()->get();
			
		}
		
		return view('showings.schedule',compact('schedule','user_type'));
	}

	public function editViewingTime($schedule)
	{
		$schedule = ViewingSchedule::find($schedule);
		$properties = Profile::where('email',Auth::user()->username)->with('renters')->get();
		return view('showings.viewing_time',compact('schedule','properties'));
	}

	public function updateViewingTime(Request $request)
	{
		ViewingSchedule::where('id',$request->schedule_id)->update(['date'=> $request->date,
			'time_from'=> $request->time_from, 'time_to' => $request->time_to, 'rooms' => $request->rooms, 'address' => $request->address]);

		$data = array(
	        'date' => date('l jS \of F Y', strtotime($request->date)),
	        'time_from' => $request->time_from,
	        'time_to' => $request->time_to,
	        'status' => "Updated",
	    );

		$schedule = ViewingSchedule::find($request->schedule_id);
	    $schedule->renters()->detach($request->previous_user);
	    $schedule->renters()->attach($request->user);

	    if($request->user){
	    	$user = User::find($request->user);
	    	$own_email = Auth::user()->username;

		    Mail::send('emails.reminder', $data, function ($message) use ($user, $own_email) {

		        $message->from($own_email, 'Room Rental Club');

		        $message->to($user->username)->subject('Status of your schedule');

		    });
	    }

		return redirect()->route('schedule');
	}

	public function deleteSchedule(Request $request,$schedule)
	{
		$schedule = ViewingSchedule::find($schedule);
		$schedule->renters()->detach($request->renter);
		Auth::user()->schedule()->detach($schedule->id);
		$schedule->delete();

		$data = array(
	        'date' => date('l jS \of F Y', strtotime($schedule->date)),
	        'time_from' => $schedule->time_from,
	        'time_to' => $schedule->time_to,
	        'status' => "Cancelled",
	    );

	    if($request->renter){
	    	$user = User::find($request->renter);
	    	$own_email = Auth::user()->username; 

		    Mail::send('emails.reminder', $data, function ($message) use ($user,$own_email) {

		        $message->from($own_email, 'Room Rental Club');

		        $message->to($user->username)->subject('Status of your schedule');

		    });
		}

		return redirect()->route('schedule');
	}

}