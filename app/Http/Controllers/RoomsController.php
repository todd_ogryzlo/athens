<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Room;
use Validator;
use App\ProspectiveRenter;
use App\User;
use Hash;
use Mail;
use Session;

class RoomsController extends Controller
{
    //
    public function checkAvailableRooms($client_id, Request $request)
    {
        $dateFrom = $request->input('dateFrom');
        $dateTo = $request->input('dateTo');
        $client = new Client();
        $room = new Room();

        $data = [];
        $data['dateFrom'] = $dateFrom;
        $data['dateTo'] = $dateTo;
        $data['rooms']= $room->getAvailableRooms($dateFrom, $dateTo);
        $data['client'] = $client->find($client_id);

        return view('rooms/checkAvailableRooms', $data);
    }

    public function roomiesRoom()
    {
        return view('rooms.roomies_form');
    }

    public function prospectiveRenters(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'fname' => 'required',
            'email' => 'required|email|unique:prospective_renters,email',

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput($request->all());
        }else{

            $pros_renters = new ProspectiveRenter();

            $pros_renters->first_name = $request->fname;
            $pros_renters->last_name = $request->lname;
            $pros_renters->tel_num = $request->tel_num;
            $pros_renters->email = $request->email;
            $pros_renters->interested_city = $request->interested_city;
            $pros_renters->interested_room = $request->interested_room;
            $pros_renters->showing_date = $request->showing_date;
            $pros_renters->showing_time = $request->showing_time;
            $pros_renters->move_date = $request->move_date;
            $pros_renters->job_title = $request->job_title;
            $pros_renters->good_time = $request->good_time;
            $pros_renters->roommates = $request->roommates;
            $pros_renters->whatsapp = $request->whatsapp;
            $pros_renters->pet_info = $request->pet_info;
            $pros_renters->price_range = $request->price_range;
            $pros_renters->query = $request->query_ques;
            
            $pros_renters->save();

            $renters_login = new User();

            $renters_login->name = $request->fname;
            $renters_login->username = $request->email;
            $renters_login->password = Hash::make($request->fname.'$now');
            $renters_login->user_type = 'renter';
            $renters_login->remember_token = $request->_token;

            $renters_login->save();

            $data = array(
            'username' => $request->email,
            'password' => $request->fname.'$now',
            );

            if($request->email){

                Mail::send('emails.new_roomies', $data, function ($message) use ($request) {

                    $message->from('er.sumit8487@gmail.com', 'Room Rental Club');

                    $message->to($request->email)->subject('Login Details');

                });
            }

            Session::flash('message', 'Thanks. Successfully submit! Login with your prospective renters'); 
            Session::flash('alert-class', 'alert-success'); 

            return redirect()->route('room-home');
        }



    }

    // public function prospectiveRentersLogin(Request $request)
    // {
    //     $validator = Validator::make($request->all(),[
    //         'username' => 'required|email',
    //         'password' => 'required',
            
    //     ]);

    //     if ($validator->fails()) {
    //         return redirect()->back()
    //                     ->withErrors($validator)->with('error_code', 5)
    //                     ->withInput($request->all());
    //     }else{

    //         $login = User::where('user_type','renter')->where('username',$request->username)->where('password',$request->password)->first();
    //         $password_true = Hash::check($request->password,$login->password);

    //         if($password_true){
    //             $name = $login->name;
    //             return redirect()->route('room-home')->with('success',$name);
    //         }else{
    //             $msg = "Invalid username and password";
    //             return redirect()->route('room-home')->with('error',$msg);
    //         }
    //     }
        
    // }
}
