<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Profile;
use Validator;
use Storage;

class ProfileController extends Controller
{	

	private static $upload_directory = 'profile';

	private static $property_directory = 'property';

	public function edit_profile()
	{
		$user = Auth::user();
		$details = Profile::where('email',$user->username)->first();

		return view('profile.edit_profile',compact('user','details'));
	}

	// public function uploadPhoto(Request $request)
	// {
	// 	print_r($request);
	// }

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(),[

	        'fname'    => 'required',
	        'email' => 'required|email', 
	        
	            
	    ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput($request->all());
        }else{

        	$exist_user = Profile::where('first_name',Auth::user()->name)->where('email',Auth::user()->username)->first();

	        $profile = new Profile();
	        
        	if($exist_user){

        		Profile::where('first_name',Auth::user()->name)->where('email',Auth::user()->username)->update([
        			'last_name'=> $request->lname,
        			'gender'=> $request->gender,
        			'birth_date'=> $request->birth_date,
        			'mobile'=> $request->mob_num,
        			'address'=> $request->address,
        			'district'=> $request->district,
        			'state'=> $request->state,
        			'zipcode'=> $request->zipcode,
        		]);

        		if($request->hasFile('profile_photo')){

		        	$photo = Storage::disk('public')->put(self::$upload_directory, $request->file('profile_photo'));
		        	Profile::where('first_name',Auth::user()->name)->where('email',Auth::user()->username)->update([
		        		'photo'=> $photo,
		        	]);
		        	
		        }

		        if($request->hasFile('house_img')){

		        	$photo = Storage::disk('public')->put(self::$property_directory, $request->file('house_img'));
		        	Profile::where('first_name',Auth::user()->name)->where('email',Auth::user()->username)->update([
		        		'house_img'=> $photo,
		        	]);
		        }

		        if($request->rooms){
		        	Profile::where('first_name',Auth::user()->name)->where('email',Auth::user()->username)->update([
		        		'num_rooms'=> $request->num_rooms,
		        	]);
		        }

		        if($request->house_type){
		        	Profile::where('first_name',Auth::user()->name)->where('email',Auth::user()->username)->update([
		        		'house_type'=> $request->house_type,
		        	]);
		        }

		        if($request->required){
		        	Profile::where('first_name',Auth::user()->name)->where('email',Auth::user()->username)->update([
		        		'required_for'=> $request->required_for,
		        	]);
		        }
		        
		        if($request->house_address){
		        	Profile::where('first_name',Auth::user()->name)->where('email',Auth::user()->username)->update([
		        		'house_address'=> $request->house_address,
		        	]);
		        }

		        if($request->price){

		        	Profile::where('first_name',Auth::user()->name)->where('email',Auth::user()->username)->update([
		        		'price'=> $request->price,
		        	]);
		        }
		        if($request->available_from){
		        	Profile::where('first_name',Auth::user()->name)->where('email',Auth::user()->username)->update([
		        		'available_from'=> $request->available_from,
		        	]);
		        }
        		

        	}else{

        		$profile->first_name = $request->fname;
		        $profile->last_name = $request->lname;

		        if($request->hasFile('profile_photo')){

		        	$photo = Storage::disk('public')->put(self::$upload_directory, $request->file('profile_photo'));
		        	$profile->photo = $photo;
		        }

		        if($request->hasFile('house_img')){

		        	$photo = Storage::disk('public')->put(self::$property_directory, $request->file('house_img'));
		        	$profile->house_img = $photo;
		        }
		        


		        $profile->email = $request->email;
		        $profile->gender = $request->gender;
		        $profile->birth_date = $request->birth_date;
		        $profile->mobile = $request->mob_num;
		        $profile->address = $request->address;
		        $profile->district = $request->district;
		        $profile->state = $request->state;
		        $profile->zipcode = $request->zipcode;
		        $profile->rooms = $request->num_rooms ? $request->num_rooms : 0;
		        $profile->house_type = $request->house_type ? $request->house_type : '';
		        $profile->required = $request->required_for ? $request->required_for : '';
		        $profile->house_address = $request->house_address ? $request->house_address : '';
		        $profile->price = $request->price ? $request->price : '' ;
		        $profile->available_from = $request->available_from ? $request->available_from : '';

		        $profile->save();
        	}



	        

	        return redirect()->route('profile');

	    }
	}
}