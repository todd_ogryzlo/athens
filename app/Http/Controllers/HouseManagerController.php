<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use Session;
use Validator;

class HouseManagerController extends Controller
{
	public function houseManagerReg(Request $request)
    {
        $validator = Validator::make($request->all(),[
        	'fname'    => 'required',
            'email' => 'required|email|unique:login,username',
            'password' => 'required',
            
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)->with('house_reg_error_code', 5)
                        ->withInput($request->all());
        }else{

            $house_manager_login = new User();

            $house_manager_login->name = $request->fname;
            $house_manager_login->username = $request->email;
            $house_manager_login->password = Hash::make($request->password);
            $house_manager_login->user_type = 'house-manager';
            $house_manager_login->remember_token = $request->_token;
            
            $house_manager_login->save();

            $msg = "Register Successfully!";
            return redirect()->route('room-home')->with('house_reg_success',$msg);
            
        }
        
    }


	// public function houseManagerLogin(Request $request)
 //    {   
 //        $validator = Validator::make($request->all(),[
 //            'username' => 'required|email',
 //            'password' => 'required',
            
 //        ]);

 //        if ($validator->fails()) {
 //            return redirect()->back()
 //                        ->withErrors($validator)->with('house_log_error_code', 5)
 //                        ->withInput($request->all());
 //        }else{

 //            $login = User::where('user_type','house-manager')->where('username',$request->username)->first();
 //            $password_true = Hash::check($request->password,$login->password);

 //            if($password_true){
                
 //                $name = $login->name;
 //                return redirect('viewing-time');
 //            }else{
                
 //                $msg = "Invalid username and password";
 //                return redirect()->route('room-home')->with('house_log_error',$msg);
 //            }
 //        }
        
 //    }


}	