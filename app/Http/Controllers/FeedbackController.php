<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feedback;
use Validator;

class FeedbackController extends Controller
{

	public function index()
	{
		return view('rooms.feedback');
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(),[
            'fname' => 'required',
            'bathroom' => 'required',
            'kitchen' => 'required',
            'living_room' => 'required',
            'lawn_cutting' => 'required',
            'cleaning_comment' => 'required',
            'toilet_cleaning' => 'required',
            'needed' => 'required',
            'supplies_needed' => 'required',
            'suggestion' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput($request->all());
        }else{

			$feedback = new Feedback();

			$feedback->first_name = $request->fname;
			$feedback->bathroom_score = $request->bathroom;
			$feedback->kitchen_score = $request->kitchen;
			$feedback->living_room_score = $request->living_room;
			$feedback->lawn_cutting_score = $request->lawn_cutting;
			$feedback->cleaning_comment = $request->cleaning_comment;
			$feedback->toilet_cleaning = $request->toilet_cleaning;
			$feedback->required = implode(',',$request->needed);
			$feedback->supplies_needed = $request->supplies_needed;
			$feedback->suggestion = $request->suggestion;

			$feedback->save();

			return redirect()->route('room-home');
		}
	}
}