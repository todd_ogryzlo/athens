<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'login';

    protected $fillable = [
        'name', 'username', 'password', 'user_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function schedule(){

        return $this->belongsToMany('App\ViewingSchedule', 'schedule_manager');
    }

    public function renters_schedule(){

        return $this->belongsToMany('App\ViewingSchedule', 'schedule_renter', 'user_id', 'schedule_id');
    }

    public function property()
    {
        return $this->belongsToMany('App\Profile','property_renter');
    }

}
