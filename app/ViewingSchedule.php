<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewingSchedule extends Model
{
	protected $table = 'viewing_schedule';

	public function manager(){

        return $this->belongsToMany('App\User', 'schedule_manager', 'viewing_schedule_id', 'user_id');
    }

    public function renters(){

        return $this->belongsToMany('App\User', 'schedule_renter', 'schedule_id', 'user_id');
    }

}