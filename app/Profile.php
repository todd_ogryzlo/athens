<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
	protected $table = 'profile';

	public function renters()
    {
        return $this->belongsToMany('App\User','property_renter', 'profile_id', 'user_id');
    }

}